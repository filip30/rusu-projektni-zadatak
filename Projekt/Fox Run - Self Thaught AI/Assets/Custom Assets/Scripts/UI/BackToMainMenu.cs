﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BackToMainMenu: MonoBehaviour {
	// Start is called before the first frame update
	void Start() {}

	// Update is called once per frame
	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			if(Time.timeScale == 0)
				Time.timeScale = 1;

			SceneManager.LoadScene("MainMenuScene", LoadSceneMode.Single);
		}
	}
}
