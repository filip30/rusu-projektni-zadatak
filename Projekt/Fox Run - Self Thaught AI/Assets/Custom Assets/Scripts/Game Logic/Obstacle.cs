﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle: MonoBehaviour {
	void OnTriggerEnter(Collider col) {
		if(col.gameObject.tag == "Fox") {
			AbstractFox FoxInTrouble = col.GetComponent<AbstractFox>();
			FoxInTrouble.Die();
		}
	}
}
