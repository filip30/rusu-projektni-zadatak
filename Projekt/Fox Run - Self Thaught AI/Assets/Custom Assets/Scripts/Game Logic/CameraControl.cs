﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl: MonoBehaviour {
	// Attributes
	public GameObject Camera;

	private const float MaxCameraHeight = 1.1f;
	private const float MinCameraHeight = -1423.9f;
	private const float CameraSpeed = 100.0f;

	// Start is called before the first frame update
	void Start() {}

	// Update is called once per frame
	void Update() {
		float CurrentCameraHeight = Camera.transform.position.y;

		if(Input.GetKey(KeyCode.W) && CurrentCameraHeight < MaxCameraHeight)
			Camera.transform.position = new Vector3(15.0f, CurrentCameraHeight + CameraSpeed * Time.deltaTime, -10.0f);
		else if(Input.GetKey(KeyCode.S) && CurrentCameraHeight > MinCameraHeight)
			Camera.transform.position = new Vector3(15.0f, CurrentCameraHeight - CameraSpeed * Time.deltaTime, -10.0f);
	}
}
