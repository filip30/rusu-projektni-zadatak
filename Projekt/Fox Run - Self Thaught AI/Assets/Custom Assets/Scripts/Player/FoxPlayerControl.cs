﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FoxPlayerControl: AbstractFox {
	// Atributes
	public Animator foxAnimator;
	public Rigidbody foxRigidbody;
	public TextMesh scoreText;
	public float jumpForce;
	public Transform CollidingObject;
	private bool isOverObstacle;
	public string PlayerName;
	private uint Points;

	void Start() {
		this.foxRigidbody = transform.GetComponent<Rigidbody>();
		this.foxAnimator = transform.GetComponentInChildren<Animator>();
		this.Points = 0;
	}

	void Update() {
		RaycastHit hit;

		scoreText.text = PlayerName + ": " + Points.ToString();

		if(Input.GetKeyDown(KeyCode.Space) && transform.localPosition.y <= 1.1)
			foxRigidbody.AddForce(transform.up * jumpForce * 5, ForceMode.Impulse);

		if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit)) {
			CollidingObject = hit.transform;
			if(hit.transform.gameObject.tag == "Obstacle" && isOverObstacle == false) {
				this.Points += 1;
				isOverObstacle = true;
			}
			else if(hit.transform.gameObject.tag != "Obstacle" && isOverObstacle == true) {
				isOverObstacle = false;
			}
		}
	}

	public override void Die() {
		Debug.Log(this.PlayerName + " is kil...");
		Time.timeScale = 0;
	}
}
